import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Product from '../components/Product'
import { listProducts } from '../actions/productAction'

const HomeScreen = () => {
  const dispatch = useDispatch()

  const productList = useSelector((state) => state.productList)
  const { loading, error, products } = productList

  useEffect(() => {
    dispatch(listProducts())
  }, [dispatch])

  return (
    <div className='container py-5'>
      <h1>Latest Products</h1>
      {loading ? (
        <div className='spinner-border' role='status'>
          <span className='sr-only'>Loading...</span>
        </div>
      ) : error ? (
        <h3>{error}</h3>
      ) : (
        <div>
          <div className='row'>
            {products.map((product) => (
              <div
                key={product._id}
                className='col col-xs-12 col-sm-12 col-md-6 col-lg-4'
              >
                <Product product={product} />
              </div>
            ))}
          </div>
        </div>
      )}
    </div>
  )
}

export default HomeScreen
