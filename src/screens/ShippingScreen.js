import React from 'react'

const ShippingScreen = () => {
  return (
    <div className='py-5'>
      <div class='alert alert-success' role='alert'>
        Order Placed Successfully !
      </div>
    </div>
  )
}

export default ShippingScreen
