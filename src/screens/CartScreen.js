import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import {
  addToCart,
  removeFromCart,
  removeAllFromCart,
} from '../actions/cartActions'

const CartScreen = ({ match, location, history }) => {
  const productId = match.params.id
  const qty = location.search ? Number(location.search.split('=')[1]) : 1

  const dispatch = useDispatch()

  const cart = useSelector((state) => state.cart)
  const { cartItems } = cart
  const tax = (
    cartItems.reduce((acc, item) => acc + item.qty * item.price, 0) *
    (1.23 / 100)
  ).toFixed(2)

  let totalItems = 0

  for (let i = 0; i < cartItems.length; i++) {
    totalItems = totalItems + Number(cartItems[i].qty)
  }

  const shipping = 500
  const subtotal =
    Number(cartItems.reduce((acc, item) => acc + item.qty * item.price, 0)) +
    Number(tax) +
    (tax > 0 ? shipping : 0)

  useEffect(() => {
    if (productId) {
      dispatch(addToCart(productId, qty))
    }
  }, [dispatch, productId, qty])

  const removeFromCartHarndler = (id) => {
    dispatch(removeFromCart(id))
  }
  const removeAllFromCartHandler = () => {
    dispatch(removeAllFromCart())
  }

  const checkoutHandler = () => {
    history.push('/shipping')
  }
  return (
    <div className='py-5'>
      <div className='row'>
        <div className='col-md-8'>
          <h1>Shopping Cart</h1>
          {/* If Cart is empty then show messege else show items */}
          {cartItems.length === 0 ? (
            <div className='alert alert-primary' role='alert'>
              Your cart is empty <Link to='/'>Go Back</Link>
            </div>
          ) : (
            <ul className='list-group'>
              {cartItems.map((item) => (
                <li key={item.product} className='list-group-item'>
                  <div className='container'>
                    <div className='row'>
                      <div className='col-md-2'>
                        <img
                          style={{ width: '60px', height: '60px' }}
                          src={item.image}
                          alt={item.name}
                        ></img>
                      </div>
                      <div className='col-md-3'>
                        <Link to={`/product/${item.product}`}>{item.name}</Link>
                      </div>
                      <div className='col-md-2'>LKR {item.price}</div>
                      <div className='col-md-2'>
                        <select
                          className='form-control'
                          value={item.qty}
                          onChange={(e) =>
                            dispatch(
                              addToCart(item.product, Number(e.target.value))
                            )
                          }
                        >
                          <option value='1'>1</option>
                          <option value='2'>2</option>
                          <option value='3'>3</option>
                          <option value='4'>4</option>
                          <option value='5'>5</option>
                        </select>
                      </div>
                      <div className='col-md-2'>
                        <button
                          className='btn-light'
                          onClick={() => removeFromCartHarndler(item.product)}
                        >
                          <i className='fa fa-trash'></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </li>
              ))}
            </ul>
          )}
          <button
            style={{ float: 'right' }}
            className='btn-dark'
            onClick={() => removeAllFromCartHandler()}
          >
            Delete All
          </button>
        </div>
        <div className='col-md-4'>
          <div className='card' style={{ width: '18rem' }}>
            <ul className='list-group'>
              <li className='list-group-item'>
                <h3>Total ({totalItems}) items</h3>
              </li>
              {cartItems.map((item) => (
                <li key={item.name} className='list-group-item'>
                  <div className='row'>
                    <div className='col-md-8'>
                      {item.name} {item.price} x {item.qty} {' = '}
                    </div>
                    <div className='col-md-4'>
                      {' LKR '}
                      {(item.price * item.qty).toFixed(2)}
                    </div>
                  </div>
                </li>
              ))}
              <li className='list-group-item'>Tax - LKR {tax}</li>
              <li className='list-group-item'>
                {Number(tax) > 0
                  ? 'Shipping - LKR  ' + shipping
                  : 'Shipping - LKR ' + 0}
              </li>
              <li className='list-group-item'>
                Sub Total - LKR {subtotal.toFixed(2)}
              </li>
              <li className='list-group-item'>
                <button
                  className='btn-block btn-primary'
                  disabled={cartItems.length === 0}
                  onClick={checkoutHandler}
                >
                  Pay now{' '}
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CartScreen
