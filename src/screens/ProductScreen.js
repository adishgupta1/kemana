import { React, useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { listProductDetails } from '../actions/productAction'

const ProductScreen = ({ history, match }) => {
  const [qty, setQty] = useState(1)

  const dispatch = useDispatch()

  const productDetails = useSelector((state) => state.productDetails)
  const { loading, error, product } = productDetails

  useEffect(() => {
    dispatch(listProductDetails(match.params.id))
  }, [dispatch, match])

  const addToCartHandler = () => {
    history.push(`/cart/${match.params.id}?qty=${qty}`)
  }

  return (
    <>
      <Link className='btn btn-dark my-3' to=''>
        Back
      </Link>

      {loading ? (
        <div class='spinner-border' role='status'>
          <span class='sr-only'>Loading...</span>
        </div>
      ) : error ? (
        error
      ) : (
        <div className='container'>
          <div className='row'>
            <div className='col col-md-6 fluid'>
              <img
                src={product.image}
                style={{ width: '400px' }}
                alt={product.image}
              ></img>
            </div>
            <div className='col-md-3'>
              <ul class='list-group'>
                <li class='list-group-item'>
                  <h3>{product.name}</h3>
                </li>
                <li class='list-group-item'>Price: LKR {product.price}</li>
                <li class='list-group-item'>
                  Description: {product.description}
                </li>
                <li class='list-group-item'>
                  <div className='row'>
                    <div className='col'>Qty:</div>
                    <div className='col'>
                      <select
                        className='form-control'
                        value={qty}
                        onChange={(e) => setQty(e.target.value)}
                        name='qtySelect'
                        id='qtySelect'
                      >
                        <option value='1'>1</option>
                        <option value='2'>2</option>
                        <option value='3'>3</option>
                        <option value='4'>4</option>
                        <option value='5'>5</option>
                      </select>
                    </div>
                  </div>
                </li>
                <li class='list-group-item'>
                  <button
                    onClick={addToCartHandler}
                    type='button'
                    className='btn-block btn-primary'
                  >
                    Add to cart
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      )}
    </>
  )
}

export default ProductScreen
