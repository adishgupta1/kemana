import React from 'react'
import Header from './components/Header'
import Footer from './components/Footer'
import HomeScreen from './screens/HomeScreen'
import ProductScreen from './screens/ProductScreen'
import CartScreen from './screens/CartScreen'

import { BrowserRouter as Router, Route } from 'react-router-dom'
import ShippingScreen from './screens/ShippingScreen'

function App() {
  return (
    <Router>
      <div className='container'>
        <Header />
        <Route path='/' component={HomeScreen} exact />
        <Route path='/product/:id' component={ProductScreen} />
        <Route path='/cart/:id?' component={CartScreen} />
        <Route path='/shipping' component={ShippingScreen} />

        <Footer />
      </div>
    </Router>
  )
}

export default App
