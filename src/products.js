const products = [
  {
    _id: '1',
    name: 'Adidas Inspired',
    image: '/images/ADIDAS SPORT INSPIRED.jpg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lectus mi, semper at tristique sed, aliquam pellentesque nulla. Vivamus at urna sem.',
    brand: 'Adidas',
    category: 'Footware',
    price: 2289.99,
  },
  {
    _id: '2',
    name: 'Afrojack Running',
    image: '/images/AFROJACK Mens running.jpg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lectus mi, semper at tristique sed, aliquam pellentesque nulla. Vivamus at urna sem.',
    brand: 'AFROJACK',
    category: 'Footware',
    price: 5599.99,
  },
  {
    _id: '3',
    name: 'Mi Athleisure',
    image: '/images/Mi Athleisure Shoes.jpg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lectus mi, semper at tristique sed, aliquam pellentesque nulla. Vivamus at urna sem.',
    brand: 'MI',
    category: 'Footware',
    price: 5929.99,
  },
  {
    _id: '4',
    name: 'Moon Shoes',
    image: '/images/Nike Moon Shoes.jpg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lectus mi, semper at tristique sed, aliquam pellentesque nulla. Vivamus at urna sem.',
    brand: 'Nike',
    category: 'Footware',
    price: 8399.99,
  },
  {
    _id: '5',
    name: 'Saucony Shoes',
    image: '/images/SauconyMensRunningShoes.jpg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lectus mi, semper at tristique sed, aliquam pellentesque nulla. Vivamus at urna sem.',
    brand: 'Saucony',
    category: 'Footware',
    price: 1249.99,
  },
  {
    _id: '6',
    name: 'Tim Casual',
    image: '/images/TimCasualShoes.jpeg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lectus mi, semper at tristique sed, aliquam pellentesque nulla. Vivamus at urna sem.',
    brand: 'Tim',
    category: 'Footware',
    price: 1829.99,
  },
  {
    _id: '7',
    name: 'Adidas Black',
    image: '/images/aadi-black-original-sports.jpeg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lectus mi, semper at tristique sed, aliquam pellentesque nulla. Vivamus at urna sem.',
    brand: 'Adidas',
    category: 'Footware',
    price: 2129.99,
  },
  {
    _id: '8',
    name: 'Oricum Training',
    image: '/images/Oricum-Running.jpeg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lectus mi, semper at tristique sed, aliquam pellentesque nulla. Vivamus at urna sem.',
    brand: 'Oricum',
    category: 'Footware',
    price: 2629.99,
  },
  {
    _id: '9',
    name: 'Satan Shoes',
    image: '/images/SatanShoes.jpeg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lectus mi, semper at tristique sed, aliquam pellentesque nulla. Vivamus at urna sem.',
    brand: 'Tim',
    category: 'Footware',
    price: 1929.99,
  },
  {
    _id: '10',
    name: 'Sparx Casual',
    image: '/images/TimCasualShoes.jpeg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lectus mi, semper at tristique sed, aliquam pellentesque nulla. Vivamus at urna sem.',
    brand: 'Sparx',
    category: 'Footware',
    price: 29.99,
  },
]

export default products
