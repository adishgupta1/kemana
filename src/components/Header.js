import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'

const Header = () => {
  const cart = useSelector((state) => state.cart)
  const { cartItems } = cart
  let totalItems = 0

  for (let i = 0; i < cartItems.length; i++) {
    totalItems = totalItems + Number(cartItems[i].qty)
  }
  function myFunction() {
    var x = document.getElementById('myTopnav')
    if (x.className === 'topnav') {
      x.className += ' responsive'
    } else {
      x.className = 'topnav'
    }
  }
  return (
    <header>
      <div className='topnav container' id='myTopnav'>
        <Link to='/' className='active'>
          Home
        </Link>
        <Link style={{ float: 'right' }} to='/cart'>
          <i className='fa fa-shopping-cart'></i>Cart {'(' + totalItems + ')'}
        </Link>
        <a href='javascript:void(0);' className='icon' onClick={myFunction}>
          <i className='fa fa-bars'></i>
        </a>
      </div>
    </header>
  )
}

export default Header
