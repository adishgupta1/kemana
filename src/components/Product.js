import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { addToCart } from '../actions/cartActions'

const Product = ({ product }) => {
  const [qty, setQty] = useState(1)

  const dispatch = useDispatch()
  const addItemToCartHandler = (event) => {
    dispatch(addToCart(product._id, qty))
  }

  const updateQty = (event) => {
    setQty(event.target.value)
  }
  return (
    <div className='card my-3' style={{ width: '18rem' }}>
      <Link to={`/product/${product._id}`}>
        <img
          className='card-img-top'
          src={product.image}
          alt='Card image cap'
          height='100px'
          width='100px'
        />
        <div className='card-body'>
          <h5 className='card-title'>{product.name}</h5>
          <p className='card-text'>LKR {product.price}</p>
        </div>
      </Link>

      <div>
        {' '}
        <button className='btn-primary mx-3' onClick={addItemToCartHandler}>
          Add To Cart
        </button>
        <select
          value={qty}
          onChange={updateQty}
          id={'productQtySelect' + product._id}
          key={product._id}
        >
          <option value='1'>1</option>
          <option value='2'>2</option>
          <option value='3'>3</option>
          <option value='4'>4</option>
          <option value='5'>5</option>
        </select>
      </div>
    </div>
  )
}

export default Product
